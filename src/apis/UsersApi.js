var express = require('express');
var router = express.Router();
var User = require("../model/User");


//For fetching user profile
router.get("/profile", function(req, res, next){
    // call the correct controller specified by the http request
    User.findOne({_id: req.query.id}, function(err, result){
        if (err){
            res.json({ confirmation: 'fail',
                message: 'Not found'
            });
            return;
        }
        
        res.json({ confirmation: 'success',
            message: result
        });
    });
});

//For user authenticaiton 
router.get("/login", function(req, res, next){
    // call the correct controller specified by the http request
    User.findOne({email: req.query.email, password: req.query.password}, function(err, result){
        if (err){
            res.json({ confirmation: 'fail',
                message: 'Not found'
            });
            return;
        }
        
        res.json({ confirmation: 'success',
            message: result
        });
    });
});

//For user registration 
router.post("/register", function(req, res, next){
    
    User.create(req.body, function(err, result) {
        if (err){
            res.json({ confirmation: 'fail',
                message: err
            });
            console.log("error inside register api",err);
            return;
        }
        
        res.json({ confirmation: 'success',
            message: result
        });
    });
});

//For user profile update 
router.put("/update", function(req, res, next){
    console.log("::: user update - ",req.body);
    User.findByIdAndUpdate(req.body.id, req.body, {new:true}, function(err, result){
        if (err){
            res.json({ confirmation: 'fail',
                message: err
            });
            console.log(err);
            return;
        }
        
        res.json({ confirmation: 'success',
            message: result
        });
    });
});

module.exports = router;