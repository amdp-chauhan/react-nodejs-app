// src/components/Flag.js
import React from 'react';

const data = {
  'cu': {
    'name': 'Cuba',
    'icon': 'flag-cu.png',
  },
  'fr': {
    'name': 'France',
    'icon': 'flag-fr.png',
  },
  'jp': {
    'name': 'Japan',
    'icon': 'flag-jp.png',
  },
  'nl': {
    'name': 'Netherlands',
    'icon': 'flag-nl.png',
  },
  'uz': {
    'name': 'Uzbekistan',
    'icon': 'flag-uz.png',
  }
};

export default class Flag extends React.Component {
  render() {
    // console.log(":: flag name - ",this.props.code);
    const name = data[this.props.code.id]?data[this.props.code.id].name:'Cuba';
    const icon = data[this.props.code.id]?data[this.props.code.id].icon:'flag-uz.png';
    return (
      <span className="flag">
        <img className="icon" title={name} src={`/img/${icon}`}/>
        {this.props.showName && <span className="name"> {name}</span>}
      </span>
    );
  }
}