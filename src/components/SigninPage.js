import React from 'react';
import axios from 'axios';

export default class SigninPage extends React.Component {
  		
	formSubmit(event){
		event.preventDefault();
		axios.get('/api/user/login', {
			params: {
				email: this.refs.email.value,
				password: this.refs.password.value
		    }
		})
		.then( (response)=> {
			console.log("::: response ",response);
			if(response.data.confirmation=="success"&&response.data.message){
				localStorage.setItem('currentUserId', response.data.message._id);
				localStorage.setItem('currentUserEmail', response.data.message.email);
				localStorage.setItem('currentUserName', response.data.message.name);
				localStorage.setItem('currentUserDateOfBirth', response.data.message.dateOfBirth);
				console.log("Successfully LoggedIn!",this)
				window.location.href = '/'
			}else alert("User not found")
		  	$('.form-control').val("")
		})
		.catch(function (error) {
		  console.log("::: Register Error ",error);
			alert("Something went wrong! Can not register this user")	
		});
	}

  	render() {
	    return (
	    	<div className="container">
		    	<div className="row">
		    		<div className="col-8">
		    			<br/>
			    		<form onSubmit={this.formSubmit.bind(this)}>
						  <div className="form-group">
						    <label htmlFor="email">Email address:</label>
						    <input type="email" className="form-control" id="email" ref="email" required />
						  </div>
						  <div className="form-group">
						    <label htmlFor="password">Password:</label>
						    <input type="password" className="form-control" id="password" ref="password" required />
						  </div>
						  <button type="submit" className="btn btn-success">Submit</button>
						</form>
		    		</div>
		    	</div>
		    </div>
	    );
  	}
}

