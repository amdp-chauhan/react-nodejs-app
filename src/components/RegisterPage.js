import React from 'react';
import axios from 'axios';

export default class RegisterPage extends React.Component {

	formSubmit(event){
		event.preventDefault();
		
		axios.post('/api/user/register', {
			name: this.refs.name.value,
			email: this.refs.email.value,
			password: this.refs.password.value,
			dateOfBirth: this.refs.dateOfBirth.value
		})
		.then(function (response) {
			if(response.data.confirmation=="success"){
				localStorage.setItem('currentUserId', response.data.message._id);
				localStorage.setItem('currentUserEmail', response.data.message.email);
				localStorage.setItem('currentUserName', response.data.message.name);
				localStorage.setItem('currentUserDateOfBirth', response.data.message.dateOfBirth);
				alert("Successfully Registered!")	
				window.location.href = '/'
			}
			alert(" Cant not register this user");
		  $('.form-control').val("")
		})
		.catch(function (error) {
		  console.log("::: Register Error ",error);
			alert("Something went wrong! Can not register this user")	
		});
	}

  render() {
    return (
    	<div className="container">
	    	<div className="row">
	    		<div className="col-8">
		    		<br/>
			    	<form onSubmit={this.formSubmit.bind(this)}>
						  <div className="form-group">
						    <label htmlFor="name">Name:</label>
						    <input type="name" className="form-control" id="name" ref="name" required />
						  </div>
						  <div className="form-group">
						    <label htmlFor="email">Email address:</label>
						    <input type="email" className="form-control" id="email" ref="email" required />
						  </div>
						  <div className="form-group">
						    <label htmlFor="dateOfBirth">Date Of Birth</label>
						    <input type="date" className="form-control" id="dateOfBirth" ref="dateOfBirth" required />
						  </div>
						  <div className="form-group">
						    <label htmlFor="password">Password:</label>
						    <input type="password" className="form-control" id="password" ref="password" required />
						  </div>
						  <button type="submit" className="btn btn-success">Submit</button>
						</form>
	    		</div>
	    	</div>
	    </div>
    );
	}
}

