import React from 'react';
import axios from 'axios';

export default class ProfilePage extends React.Component {
  	constructor(props){
  		super(props);
  		this.state = {
			currentUserId: "",
			currentUserEmail: "",
			currentUserName: "",
			currentUserDateOfBirth: ""
  		}
  	}

  	componentDidMount(){
  		this.setState({
  			currentUserId: localStorage.getItem('currentUserId')||false,
  			currentUserEmail: localStorage.getItem('currentUserEmail')||false,
  			currentUserName: localStorage.getItem('currentUserName')||false,
  			currentUserDateOfBirth: localStorage.getItem('currentUserDateOfBirth')||false,
  		})
  	}

	formSubmit(event){
		event.preventDefault();

		axios.put('/api/user/update', {
			id: localStorage.getItem('currentUserId'),
			name: this.refs.name.value,
			email: this.refs.email.value,
			dateOfBirth: this.refs.dateOfBirth.value
		})
		.then(function (response) {
			console.log(":: response - > ",response);
			if(response.data.confirmation=="success"&&response.data.message){
				localStorage.setItem('currentUserId', response.data.message._id);
				localStorage.setItem('currentUserEmail', response.data.message.email);
				localStorage.setItem('currentUserName', response.data.message.name);
				localStorage.setItem('currentUserDateOfBirth', response.data.message.dateOfBirth);
				alert("Successfully Registered!")	
				location.reload();
			}

		  $('.form-control').val("")
		})
		.catch(function (error) {
		  console.log("::: Register Error ",error);
			alert("Something went wrong! Can not register this user")	
		});
	}

	formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;

	    return [year, month, day].join('-');
	}

  	render() {
  		let currentUser = this.state.currentUser;
	    return (
	    	<div className="container">
		    	<div className="row">
		    		<div className="col-8">
			    		<br/>
				    	<form onSubmit={this.formSubmit.bind(this)}>

							<div className="form-group">
							    <label htmlFor="name">Name:</label>
							    <input type="name" className="form-control" ref="name" id="name" value={this.state.currentUserName} onChange={(e)=>this.setState({currentUserName: e.target.value})}required />
							</div>
							<div className="form-group">
							    <label htmlFor="email">Email address:</label>
							    <input type="email" className="form-control" ref="email" id="email" value={this.state.currentUserEmail} disabled required />
							</div>
							<div className="form-group">
							    <label htmlFor="dateOfBirth">Date Of Birth</label>
							    <input type="date" className="form-control" ref="dateOfBirth" id="dateOfBirth" value={this.formatDate(this.state.currentUserDateOfBirth)} onChange={(e)=>this.setState({currentUserDateOfBirth: e.target.value})} required />
							</div>
							<button type="submit" className="btn btn-success">Submit</button>
							</form>
		    		</div>
		    	</div>
		    </div>
	    );
  	}
}

