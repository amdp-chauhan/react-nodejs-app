import React from 'react';
import axios from 'axios';

export default class Navigation extends React.Component {
  	constructor(props){
  		super(props);
  		this.state = {
  			currentEmail: ""
  		}
  	}

  	logout(event){
  		event.preventDefault();
  		window.localStorage.clear();
		window.location.href = '/signin'
  	}

  	componentDidMount(){
  		this.setState({currentEmail: localStorage.getItem('currentUserEmail')||false})
  	}

  	render() {

  		let email = this.state.currentEmail;
  		return (
			<div className="topnav" id="myTopnav">
			  { email ? <a href="/">Home</a> : '' }
			  { email ? <a href="/profile">Profile</a> : '' }
			  { email ? '' : <a href="/register">Register</a> }
			  { email ? '' : <a href="/signin">Sign-In</a> }
			  { email ? <a href="#" onClick={this.logout.bind(this)}>Logout</a> : '' }
			</div>
	    );
  	}
}

