// src/components/Layout.js
import React from 'react';
import { Link } from 'react-router';
import Navigation from './Navigation'
export default class Layout extends React.Component {
  /*componentDidMount(){
    console.log("::localStorage.getItem('currentUserEmail') ",localStorage.getItem('currentUserEmail'))
    if(!localStorage.getItem('currentUserEmail'))
      window.location.href = '/signin'

  }*/

  render() {
    return (
      <div className="app-container">
        <header>
          <Navigation />
        </header>
        <div className="app-content">{this.props.children}</div>
        <footer>
        </footer>
      </div>
    );
  }
}