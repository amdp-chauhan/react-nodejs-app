// src/routes.js
import React from 'react'
import { Route, IndexRoute } from 'react-router'
import Layout from './components/Layout';
import IndexPage from './components/IndexPage';
import AthletePage from './components/AthletePage';
import RegisterPage from './components/RegisterPage';
import SigninPage from './components/SigninPage';
import ProfilePage from './components/ProfilePage';
import NotFoundPage from './components/NotFoundPage';

const routes = (
  <Route path="/" component={Layout}>
    <IndexRoute component={IndexPage}/>
    <Route path="athlete/:id" component={AthletePage}/>
    <Route path="register" component={RegisterPage}/>
    <Route path="profile" component={ProfilePage}/>
    <Route path="signin" component={SigninPage}/>
    <Route path="*" component={NotFoundPage}/>
  </Route>
);

export default routes;