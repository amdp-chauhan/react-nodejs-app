var mongoose = require('mongoose');

//Create a schema
var UserSchema = new mongoose.Schema({
  	name: { type:String },
 	email: { type:String },
 	password: { type:String },
  	dateOfBirth: { type:Date },
 	createdAt: { type:Date, default: Date.now()}
});

module.exports = mongoose.model('users', UserSchema);
