import mongoose from 'mongoose';

// Create the database connection 
const db = mongoose.connect('mongodb://localhost:27017/users-app')


// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('error', (err)=>console.log("::: Mongo Connection Error - ",err))


// If the connection throws an error
mongoose.connection.on('connected', ()=>console.log("::: App Connected with 'users-app' DB"))


// When the connection is disconnected
mongoose.connection.on('disconnected', ()=>console.log("::: Mongo Disconnected"))


process.on('SIGINT', function(){
	mongoose.connection.close(()=>{
		console.log("::: Mongo Connection Closed");
		process.exit(0)
	})
})

export default db