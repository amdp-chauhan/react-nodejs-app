# node-react-app


## Setup

 - Install npm
	
	npm i

 - Install webpack globally if not installed previously

    npm i webpack -g


## Available commands

 - In Terminal's one tab run 
 	
 	npm start

 - In other tab run

 	webpack -w


## At last when both command runs properly, then open localhost:3000 on browser